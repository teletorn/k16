var translations =
   {
      "et": [
         {
            "first_question"  :  "Mis on<br>innovatsioon?",
            "second_question" :  "Mis on<br>Geenivaramu?",
            "third_question"  :  "Eesti<br>Geenivaramu<br>ajajoon",
            "watch_video"     :  "Vaata",
            "video_one"       :  "videos/WTB11117-EE.mp4",
            "video_two"       :  "videos/WTB11096_Short_3_-_Metspalu_ET.mp4",
            "gene_timeline"   :  "Geenivaramu ajajoon",
         }
      ],
      "en": [
         {
            "first_question"  :  "What is<br>innovation?",
            "second_question" :  "What is<br>the Genome Center?",
            "third_question"  :  "Timeline of<br>the Estonian<br>Genome Center",
            "watch_video"     :  "Play",
            "video_one"       :  "videos/WTB11117-EN.mp4",
            "video_two"       :  "videos/WTB11096_geenivaramu_EN.mp4",
            "gene_timeline"   :  "Timeline of the Genome Center",
         }
      ],
      "ru": [
         {
            "first_question"  :  "Что такое<br>инновация?",
            "second_question" :  "Что такое<br>Генный фонд?",
            "third_question"  :  "Хронология<br>Эстонского<br>генного фонда",
            "watch_video"     :  "Смотреть",
            "video_one"       :  "videos/WTB11117-RU.mp4",
            "video_two"       :  "videos/WTB11096_geenivaramu_RU.mp4",
            "gene_timeline"   :  "Хронология генного фонда",
         }
      ],
      "fi": [
         {
            "first_question"  :  "Mikä on<br>innovaatio?",
            "second_question" :  "Mikä on<br>Geenipankki?",
            "third_question"  :  "Viron<br>Geenipankin<br>aikajana",
            "watch_video"     :  "Katso",
            "video_one"       :  "videos/WTB11117-FI.mp4",
            "video_two"       :  "videos/WTB11096_geenivaramu_FI.mp4",
            "gene_timeline"   :  "Geenipankin aikajana",
         }
      ]
   }